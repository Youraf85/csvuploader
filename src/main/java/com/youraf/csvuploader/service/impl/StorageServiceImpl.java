package com.youraf.csvuploader.service.impl;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import com.youraf.csvuploader.domain.Customer;
import com.youraf.csvuploader.repository.CustomerRepository;
import com.youraf.csvuploader.service.StorageService;

@Service
@Transactional
public class StorageServiceImpl implements StorageService {

    private final Logger log = LoggerFactory.getLogger(StorageServiceImpl.class);
    
    @Autowired
    private CustomerRepository custoRepo;

    @Override
	public void store(MultipartFile multipartFile) {
		log.info("Request to store Customer File: {}", multipartFile.getOriginalFilename());

		try {
			File file = convert(multipartFile);
			String line = "";
			String cvsSplitBy = ";";
			
			try (BufferedReader br = new BufferedReader(new FileReader(file))) {

	            while ((line = br.readLine()) != null) {
	            	String[] fields = line.split(cvsSplitBy);
	                
	            	if(fields.length==5) {
		                Customer custo=new Customer(fields[0],fields[1],fields[2],fields[3],fields[4]);
		                custoRepo.save(custo);
		                log.info("Insert the customer: {}", custo.getName());
	            	}else {
	            		log.warn("The fields are not complete");
	            	} 
	            }

	        } catch (IOException e) {
	        	log.error("Error during reading gile", e.getMessage());
	        }
		} catch (IOException e) {
			log.error("Error during converting file", e.getMessage());
		}
	}

	public static File convert(MultipartFile file) throws IOException {
		File convFile = new File(file.getOriginalFilename());
		convFile.createNewFile();
		FileOutputStream fos = new FileOutputStream(convFile);
		fos.write(file.getBytes());
		fos.close();
		return convFile;
	}
}
