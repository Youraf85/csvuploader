/**
 * View Models used by Spring MVC REST controllers.
 */
package com.youraf.csvuploader.web.rest.vm;
