export interface ICustomer {
    id?: number;
    name?: string;
    city?: string;
    country?: string;
    phone?: string;
    mail?: string;
}

export class Customer implements ICustomer {
    constructor(
        public id?: number,
        public name?: string,
        public city?: string,
        public country?: string,
        public phone?: string,
        public mail?: string
    ) {}
}
