import { NgModule } from '@angular/core';

import { CsvUploaderSharedLibsModule, JhiAlertComponent, JhiAlertErrorComponent } from './';

@NgModule({
    imports: [CsvUploaderSharedLibsModule],
    declarations: [JhiAlertComponent, JhiAlertErrorComponent],
    exports: [CsvUploaderSharedLibsModule, JhiAlertComponent, JhiAlertErrorComponent]
})
export class CsvUploaderSharedCommonModule {}
